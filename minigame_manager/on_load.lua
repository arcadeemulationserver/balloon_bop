local S = core.get_translator("balloon_bop")

arena_lib.on_load("balloon_bop", function(arena)

        arena_lib.HUD_send_msg_all("title", arena, S("Pop all the balloons, don't let them touch anything else!"), 3, nil, "0xE6482E")
        balloon_bop.scores[arena.name] = balloon_bop.scores[arena.name] or {}
end)
