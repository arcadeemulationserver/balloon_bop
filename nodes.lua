local S = core.get_translator("balloon_bop")

-- you can register nodes here for your minigame.

-- given here is a solid airlike node that cant be dug, and a killer airlike node that kills players on contact

-- of course, if you want to keep these useful nodes for your minigame, change the mod name

local modname = "balloon_bop"


-- core.register_node(modname .. ":killer",{
--         description = "Minigame Killer",
--         inventory_image = "no_texture_airlike.png",
--         drawtype = "airlike",
--         paramtype = "light",
--         sunlight_propagates = true,
--         walkable     = false, 
--         pointable    = true, 
--         diggable     = false, 
--         buildable_to = false,  
--         drop = "",
--         damage_per_second = 2000,
--         groups = {},
-- })


core.register_node(modname .. ":wall",{
        description = S("Balloon Bop Blocker"),
        inventory_image = "balloon_bop_blocker.png",
        wield_image = "balloon_bop_blocker.png",
        drawtype = "airlike",
        paramtype = "light",
        sunlight_propagates = true,
        walkable     = true, 
        pointable    = true, 
        diggable     = false, 
        buildable_to = false,  
        drop = "",
        groups = {},
})



core.register_node("balloon_bop:spike", {
        description = S("Balloon Bop Spike"),
        drawtype = "plantlike",
        tiles = {"balloon_bop_spike.png"},
        paramtype = "light",
        sunlight_propagates = true,
        collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
    })
