local function update_score_hud(arena, player_name)
    local player = core.get_player_by_name(player_name)
    if not player then
        return
    end
    -- update score hud
    if balloon_bop.infohuds[player_name] then
        player:hud_change(balloon_bop.infohuds[player_name], "text", arena.players[player_name].score)
    end
    -- set player's nametag with their score
    player:set_nametag_attributes({
        text = tostring(arena.players[player_name].score),
        color = "#5A5353",
        bgcolor = false,
    })
end

function balloon_bop.add_score(arena, player_name, score)
    if not arena.players[player_name] then
       return
    end
    arena.players[player_name].score = arena.players[player_name].score + score
    update_score_hud(arena, player_name)
end

function balloon_bop.set_score(arena, player_name, score)
    if not arena.players[player_name] then
       return
    end
    arena.players[player_name].score = score
    update_score_hud(arena, player_name)
end

function balloon_bop.get_score(arena, player_name)
    if not arena.players[player_name] then
       return nil
    end
    return arena.players[player_name].score
end

function balloon_bop.show_score_particle(pos, o_texture, score, player_name)
    if score ~= 10 and score ~= 15 and score ~= 20 and score ~= -20 then
        return
    end
    local texture
    if score > 0 then
        texture="balloon_bop_point_screen_"..tostring(score)..".png^[mask:"..o_texture .. "^[opacity:170"
    else
        texture="balloon_bop_point_screen_"..tostring(math.abs(score)).."m.png^[colorize:#2C2C2C:255^[opacity:170"
    end
    -- show points particle
    core.add_particlespawner({
        amount = 1,
        time = .01,
        pos=vector.new(pos.x,pos.y+2,pos.z),
        vel=vector.new(0,1,0),
        acc = vector.new(0,0,0),
        exptime = 2.5,
        minsize = 10,
        maxsize = 13,
        collisiondetection = false,
        vertical = false,
        texture = {
            name = texture,
            alpha_tween = { start = 0.83333, 1, 0 },
        },
        playername = player_name,
    })
end
